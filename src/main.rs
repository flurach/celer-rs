mod frame;
mod opcode;
mod object;
mod vm;

use opcode::Opcode;
use object::Object;

fn main() {
	let codes = vec![
		Opcode::LoadObject(Object::string("Hello World")),
		Opcode::StoreVar("x".to_string()),

		Opcode::LoadObject(Object::function(vec![
			Opcode::LoadObject(Object::string("Hi World")),
			Opcode::StoreVar("y".to_string()),
		])),
		Opcode::RunFunction,

		Opcode::LoadObject(Object::function(vec![
			Opcode::LoadVar("x".to_string()),
			Opcode::PrintObject,
			Opcode::LoadVar("Y".to_string()),
			Opcode::PrintObject,
		])),
		Opcode::RunFunction,
	];

	vm::run(codes);
}