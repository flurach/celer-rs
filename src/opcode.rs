use super::object::Object;

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub enum Opcode {
	LoadObject(Object),
	DuplicateObject,
	PrintObject,

	Add,
	Subtract,
	Multiply,
	Divide,
	Exponent,

	RunFunction,
	ArrayPush,
	ArrayPop,
	ArrayIndex,
	ArrayLen,
	MapAdd,
	MapRemove,
	MapKeys,
	MapValues,

	ToNumber,
	ToString,
	ToBool,

	Not,
	And,
	Or,

	StoreVar(String),
	LoadVar(String),

	Jump(usize),
	JumpIfFalse(usize),

	Equal,
	Lesser,
	Greater,
	LesserEqual,
	GreaterEqual,
}