use super::opcode::Opcode;
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Clone)]
#[allow(dead_code)]
pub enum Object {
	None,
	NaN,
	Number(f64),
	String(String),
	Boolean(bool),
	Function(Rc<Vec<Opcode>>),
	Array(Rc<Vec<Object>>),
	Map(Rc<RefCell<HashMap<String, Object>>>),
}

#[allow(dead_code)]
impl Object {
	pub fn integer(n: i64) -> Object {
		Object::Number(n as f64)
	}

	pub fn float(d: f64) -> Object {
		Object::Number(d)
	}

	pub fn string(s: &str) -> Object {
		Object::String(s.to_string())
	}

	pub fn boolean(b: bool) -> Object {
		Object::Boolean(b)
	}

	pub fn function(codes: Vec<Opcode>) -> Object {
		Object::Function(Rc::new(codes))
	}

	pub fn array(values: Vec<Object>) -> Object {
		Object::Array(Rc::new(values))
	}

	pub fn map(pairs: HashMap<String, Object>) -> Object {
		Object::Map(Rc::new(RefCell::new(pairs)))
	}
}

impl Object {
	pub fn add(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Number(n1 + n2),
				_ => Object::NaN,
			},
			Object::String(s1) => match other {
				Object::String(s2) => Object::String(s1.to_owned() + s2),
				_ => Object::NaN
			},
			Object::Array(a1) => match other {
				Object::Array(a2) => {
					let mut a3 = a1.to_vec().clone();
					a3.extend(a2.to_vec());
					Object::Array(Rc::new(a3))
				},
				_ => Object::NaN
			},
			_ => Object::NaN
		}
	}

	pub fn subtract(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Number(n1 - n2),
				_ => Object::NaN
			},
			_ => Object::NaN
		}
	}

	pub fn multiply(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Number(n1 * n2),
				Object::String(s2) => Object::String(s2.repeat(*n1 as usize)),
				_ => Object::NaN
			},
			Object::String(s1) => match other {
				Object::Number(n2) => Object::String(s1.repeat(*n2 as usize)),
				_ => Object::NaN
			},
			_ => Object::NaN
		}
	}

	pub fn divide(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Number(n1 / n2),
				_ => Object::NaN
			},
			_ => Object::NaN
		}
	}

	pub fn exponent(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Number(n1.powf(*n2)),
				_ => Object::NaN
			},
			_ => Object::NaN
		}
	}
}

impl Object {
	pub fn to_bool(&self) -> bool {
		match self {
			Object::Number(n) => *n != 0.0,
			Object::String(s) => s.len() != 0,
			Object::Boolean(b) => *b,
			Object::Array(a) => a.to_vec().len() != 0,
			Object::Map(m) => m.borrow().len() != 0,
			_ => false,
		}
	}

	pub fn to_string(&self) -> String {
		format!("{}", self)
	}

	pub fn to_number(&self) -> Object {
		match self {
			Object::Number(_) => self.clone(),
			Object::String(s) => match s.parse::<f64>() {
				Ok(n) => Object::Number(n),
				Err(_) => Object::NaN
			},
			_ => Object::NaN
		}
	}
}

impl Object {
	pub fn equal(&self, other: &Object) -> Object {
		match self {
			Object::None => match other {
				Object::None => Object::Boolean(true),
				_ => Object::Boolean(false)
			},
			Object::NaN => match other {
				Object::NaN => Object::Boolean(true),
				_ => Object::Boolean(false)
			},
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Boolean(n1 == n2),
				_ => Object::Boolean(false)
			},
			Object::String(s1) => match other {
				Object::String(s2) => Object::Boolean(s1 == s2),
				_ => Object::Boolean(false)
			},
			Object::Boolean(b1) => match other {
				Object::Boolean(b2) => Object::Boolean(b1 == b2),
				_ => Object::Boolean(false)
			},
			_ => Object::Boolean(false),
		}
	}

	pub fn lesser(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Boolean(n1 < n2),
				_ => Object::NaN,
			}
			_ => Object::NaN
		}
	}

	pub fn greater(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Boolean(n1 > n2),
				_ => Object::NaN,
			}
			_ => Object::NaN
		}
	}

	pub fn lesser_equal(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Boolean(n1 <= n2),
				_ => Object::NaN,
			}
			_ => Object::NaN
		}
	}

	pub fn greater_equal(&self, other: &Object) -> Object {
		match self {
			Object::Number(n1) => match other {
				Object::Number(n2) => Object::Boolean(n1 >= n2),
				_ => Object::NaN,
			}
			_ => Object::NaN
		}
	}
}

impl std::fmt::Display for Object {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Object::None => write!(f, "None"),
			Object::NaN => write!(f, "NaN"),
			Object::Number(n) => write!(f, "{}", n),
			Object::String(s) => write!(f, "{}", s),
			Object::Boolean(b) => write!(f, "{}", b),
			Object::Function(_) => write!(f, "[function]"),
			Object::Array(a) => write!(f, "{:?}", a.to_vec()),
			Object::Map(m) => write!(f, "{:?}", m.borrow()),
		}
	}
}

impl std::fmt::Debug for Object {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Object::String(s) => write!(f, "'{}'", s),
			_ => write!(f, "{}", self),
		}
	}
}