use std::collections::HashMap;
use super::object::Object;

#[derive(Debug)]
pub struct Frame {
	vars: HashMap<String, Object>,
}

impl Frame {
	pub fn new() -> Self {
		Frame { vars: HashMap::new() }
	}

	pub fn set(&mut self, key: String, value: Object) {
		self.vars.insert(key, value);
	}

	pub fn get(&mut self, key: &String) -> Option<Object> {
		match self.vars.get(key) {
			Some(o) => Some(o.clone()),
			None => None
		}
	}
}