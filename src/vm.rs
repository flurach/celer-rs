use super::object::Object;
use super::opcode::Opcode;
use super::frame::Frame;
use std::rc::Rc;

struct State {
	frames: Vec<Frame>,
	stack: Vec<Object>,
}

impl State {
	fn push_frame(&mut self) { self.frames.push(Frame::new()) }
	fn pop_frame(&mut self) { self.frames.pop(); }

	fn set(&mut self, key: String, value: Object) {
		let last = self.frames.len() - 1;
		self.frames[last].set(key, value)
	}

	fn get(&mut self, key: &String) -> Object {
		let last = self.frames.len() - 1;
		if let Some(value) = self.frames[last].get(key) {
			return value
		}

		if self.frames.len() != 1 {
			return match self.frames[0].get(key) {
				Some(o) => o,
				None => Object::None
			}
		}

		Object::None
	}
}

pub fn run(codes: Vec<Opcode>) {
	let mut state = State { frames: vec![Frame::new()], stack: Vec::new() };
	_run(codes, &mut state);
}

fn _run(codes: Vec<Opcode>, state: &mut State) {
	let mut i = 0;

	loop {
		if i >= codes.len() { break }

		match &codes[i] {
			Opcode::LoadObject(o) => state.stack.push(o.clone()),
			Opcode::DuplicateObject => state.stack.push(state.stack.last().unwrap().clone()),
			Opcode::PrintObject => println!("{}", state.stack.pop().unwrap()),
			Opcode::Add => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.add(&second));
			},
			Opcode::Subtract => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.subtract(&second));
			},
			Opcode::Multiply => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.multiply(&second));
			},
			Opcode::Divide => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.divide(&second));
			},
			Opcode::Exponent => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.exponent(&second));
			},
			Opcode::RunFunction => {
				let f = state.stack.pop().unwrap();
				if let Object::Function(fcodes) = f {
					state.push_frame();
					_run(fcodes.to_vec(), state);
					state.pop_frame();
				}
			},
			Opcode::ArrayPush => {
				let o = state.stack.pop().unwrap();
				if let Some(Object::Array(a)) = state.stack.pop() {
					a.to_vec().push(o)
				}
			},
			Opcode::ArrayPop => {
				if let Some(Object::Array(a)) = state.stack.pop() {
					state.stack.push(a.to_vec().pop().unwrap())
				}
			},
			Opcode::ArrayIndex => {
				let i = state.stack.pop().unwrap();
				let a = state.stack.pop().unwrap();

				if let Object::Number(i) = i {
					let i = i as usize;
					if let Object::Array(a) = a {
						let a = a.to_vec();
						if i < a.len() {
							state.stack.push(a[i].clone())
						}
					}
				}
			},
			Opcode::ArrayLen => {
				if let Some(Object::Array(a)) = state.stack.pop() {
					state.stack.push(Object::Number(a.to_vec().len() as f64))
				}
			},
			Opcode::MapAdd => {
				let v = state.stack.pop().unwrap();
				if let Some(Object::String(k)) = state.stack.pop() {
					if let Some(Object::Map(m)) = state.stack.last() {
						(*m.borrow_mut()).insert(k, v);
					}
				}
			},
			Opcode::MapRemove => {
				if let Some(Object::String(k)) = state.stack.pop() {
					if let Some(Object::Map(m)) = state.stack.last() {
						(*m.borrow_mut()).remove(&k);
					}
				}
			},
			Opcode::MapKeys => {
				if let Some(Object::Map(m)) = state.stack.pop() {
					let m = m.borrow();
					let keys = m.keys().map(|k| Object::String(k.clone())).collect();
					state.stack.push(Object::Array(Rc::new(keys)))
				}
			},
			Opcode::MapValues => {
				if let Some(Object::Map(m)) = state.stack.pop() {
					let m = m.borrow();
					let keys = m.values().map(|v| v.clone()).collect();
					state.stack.push(Object::Array(Rc::new(keys)))
				}
			},
			Opcode::ToNumber => {
				let o = state.stack.pop().unwrap();
				state.stack.push(o.to_number());
			},
			Opcode::ToString => {
				let o = state.stack.pop().unwrap();
				state.stack.push(Object::String(o.to_string()));
			},
			Opcode::ToBool => {
				let o = state.stack.pop().unwrap();
				state.stack.push(Object::Boolean(o.to_bool()));
			},
			Opcode::Not => {
				let o = state.stack.pop().unwrap();
				state.stack.push(Object::Boolean(!o.to_bool()));
			},
			Opcode::And => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(Object::Boolean(first.to_bool() && second.to_bool()))
			},
			Opcode::Or => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(Object::Boolean(first.to_bool() || second.to_bool()))
			},
			Opcode::StoreVar(key) => {
				let value = state.stack.pop().unwrap();
				state.set(key.clone(), value);
			},
			Opcode::LoadVar(key) => {
				let o = state.get(key);
				state.stack.push(o);
			},
			Opcode::Jump(n) => {
				i = n - 1;
			},
			Opcode::JumpIfFalse(n) => {
				if state.stack.pop().unwrap().to_bool() == false {
					i = n - 1;
				}
			},
			Opcode::Equal => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.equal(&second));
			},
			Opcode::Lesser => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.lesser(&second));
			},
			Opcode::Greater => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.greater(&second));
			},
			Opcode::LesserEqual => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.lesser_equal(&second));
			},
			Opcode::GreaterEqual => {
				let second = state.stack.pop().unwrap();
				let first = state.stack.pop().unwrap();
				state.stack.push(first.greater_equal(&second));
			},
		}

		i += 1;
	}
}